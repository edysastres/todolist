const BASE_URL = "http://localhost:3000/";

window.addEventListener('load', function () {
    loadList();
    addTask();
    deleteTask();
})

function loadList() {
    getList()
        .then(createList)
        .catch(console.error)
}
function getList(params) {
    return fetch(`${BASE_URL}tasks`)
        .then(response => response.json());
}

function createList(tasks) {
    const list = document.getElementById('todo-list')
    for (const task of tasks) {
        const li = document.createElement('li')
        li.setAttribute("taskId", task.id);
        li.innerHTML = `
        ${task.title}<span taskId="${task.id}" class="close">x</span>
        `;
        list.appendChild(li)
    }
}

function addTask() {
    const btn = document.getElementById('btn-add');
    btn.addEventListener('click', function (event) {
        event.preventDefault();
        const intputTask = document.getElementById('taskId');
        const task = { 'title': intputTask.value }
        postTask(task)
            .then(response => {
                createTask(response);
                intputTask.value = "";
            })
            .catch(console.error)
    })
}

function postTask(task) {
    return fetch(`${BASE_URL}tasks`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(task)
    })
        .then(response => response.json())
        .catch(console.error)
}

function createTask(task) {
    const list = document.getElementById('todo-list')
    const li = document.createElement('li')
    li.setAttribute("taskId", task.id);
    li.innerHTML = `
        ${task.title}<span class="close">x</span>
        `;
    list.appendChild(li)
}

function deleteTask() {
    const btn = document.getElementsByClassName('close');
    btn.addEventListener('click', function (event) {
        event.preventDefault();
        const intputTask = document.getElementById('taskId');
        const task = { 'title': intputTask.value }
        postTask(task)
            .then(response => {
                createTask(response);
                intputTask.value = "";
            })
            .catch(console.error)
    })
}

function deleteMethodTask(task) {
    return fetch(`${BASE_URL}tasks/${task.id}`, {
        method: 'DELETE'
    })
        .then(response => response.json())
        .catch(console.error)
}